const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const WriteFilePlugin = require('write-file-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const FixStyleOnlyEntries = require("webpack-fix-style-only-entries");
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const TerserJSPlugin = require('terser-webpack-plugin');

module.exports = {
  // モード値を production に設定すると最適化された状態で、
  // development に設定するとソースマップ有効でJSファイルが出力される
  mode: 'development',
  // メインとなるJavaScriptファイル（エントリーポイント）
  entry: {
    style: './src/scss/style.scss',
    "js/main": './src/es/main.js',
  },

  devtool: 'source-map', //ソースマップツールを有効

  module: {
    rules: [
      {
        test: /\.js$/,
        use: [
          {
            loader: "babel-loader",
            options: {
              presets: [
                // ES5 に変換
                "@babel/preset-env"
              ]
            }
          }
        ]
      },
      {
        test: /\.vue$/,
        loader: "vue-loader"
      },
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader, // javascriptとしてバンドルせず css として出力する
          {
            loader: "css-loader",
            options: {
              url: false,
              sourceMap: true, //ソースマップツールを有効
            },
          },
          {
            loader: 'postcss-loader',
            options: {
                sourceMap: true, //ソースマップを有効
                plugins: [
                    require('autoprefixer')({
                        grid: true, // CSS Grid Layout を使いたいんだ
                    })
                ]
            }
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true, //ソースマップツールを有効
            },
          },
        ],
      },
    ],
  },

  // ファイルの出力設定
  output: {
    //  出力ファイルのディレクトリ名
    path: `${__dirname}/dist`,
    // 出力ファイル名
    filename: '[name].js',
  },

  resolve: {
    alias: {
        'vue$': 'vue/dist/vue.esm.js',
    }
  },

  plugins: [
    new CopyWebpackPlugin({
      patterns: [
        {
          from: `${__dirname}/src/static/**/*.html`,
          to: `${__dirname}/dist`,
          context: `${__dirname}/src/static`
        },
      ],
    }),
    new WriteFilePlugin(),
    new VueLoaderPlugin(),
    new FixStyleOnlyEntries(),
    new MiniCssExtractPlugin({
      // prefix は output.path
      filename: 'css/[name].css'
    }),
  ],

  optimization: {
    minimizer: [new TerserJSPlugin({}), new OptimizeCSSAssetsPlugin({})],
  },

  // ローカル開発用環境を立ち上げる
  // 実行時にブラウザが自動的に localhost を開く
  devServer: {
    contentBase: 'dist',
    open: true,
  },
};
