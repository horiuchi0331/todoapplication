import Vue from 'vue'

const app = new Vue({
  el: '#app',
  data() {
    return {
      items: [{
        content: '[タスク名]\nタスク内容',
        toggle: false // trueで完了
      }],
      remaining: 0,
      options: [
        { value: -1, label: 'すべて' },
        { value: 0,  label: 'アクティブ' },
        { value: 1,  label: '完了' }
      ],
      // 選択している options の value を記憶するためのデータ
      // 初期値を「-1」つまり「すべて」にする
      current: -1
    }
  },
  created() {
    let todoRef = firebase.database().ref('todo/task');
    let remainingRef = firebase.database().ref('todo/remainingTask');
    let _this = this;

    // Firebaseからデータ取得（task）
    todoRef.on('value', function(snapshot) {
      let getData = snapshot.val();

      // データがない場合はdata()初期値をFirebase側にも設定
      if(getData !== null) {
        _this.items = getData;
      } else {
        todoRef.set(_this.items);
      }
    });

    // Firebaseからデータ取得（remaining）
    remainingRef.on('value', function(snapshot) {
      let getData = snapshot.val();

      // データがない場合はdata()初期値をFirebase側にも設定
      if(getData !== null) {
        _this.remaining = getData;
      } else {
        remainingRef.set(_this.remaining);
      }
    });
  },
  computed: {
    itemsStatusFilter: function() {
      // データ current が -1 ならすべて
      // それ以外なら current と state が一致するものだけに絞り込む
      const currentStatus = this.current
      return this.items.filter(function(item, index) {
        return  currentStatus < 0 ? true : item.toggle == currentStatus
      })
    }
  },
  methods: {
    // 「追加」ボタンでタスク追加
    addItems () {
      // ページ内
      this.items.push(this.independentObejct())
      this.count++

      // 残タスク計算関数の呼び出し
      this.taskRemaining()
    },
    // 状態変更の処理
    changeState: function(value) {
      this.current = value
    },
    // 「削除」ボタンでタスク削除
    removeItems (target) {
      this.items.splice(target, 1)
      this.count--

      // 残タスク計算関数の呼び出し
      this.taskRemaining()
    },
    // 「クリア」ボタンでタスク削除
    clearItems () {
      this.items = this.items.filter(function(item, index) {
        return  item.toggle == false
      })
    },
    // 新規タスクに設定する内容
    independentObejct () {
      return {
        content: '[タスク名]\nタスク内容',
        toggle: false
      }
    },
    // 残タスク計算
    taskRemaining () {
      this.remaining = this.items.filter(function(item, index) {
        return  item.toggle == false
      }).length;
    }
  },
  watch: {
    items: {
      // 書き込みしたタスク内容をFirebaseに反映
      handler: function(){
        let todoRef = firebase.database().ref('todo/task');
        todoRef.set(this.items);
      },
      deep: true
    },
    remaining: {
      // 書き込みしたタスク内容をFirebaseに反映
      handler: function(){
        let remainingRef = firebase.database().ref('todo/remainingTask');
        remainingRef.set(this.remaining);
      },
      deep: true
    }
  }
})
